

import Foundation
import ObjectMapper

struct Links : Mappable {
	var href : String?
	var rel : String?
	var method : String?
	var encType : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		href <- map["href"]
		rel <- map["rel"]
		method <- map["method"]
		encType <- map["encType"]
	}

}
