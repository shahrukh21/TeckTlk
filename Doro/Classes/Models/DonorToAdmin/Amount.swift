

import Foundation
import ObjectMapper

class Amount : Mappable {
	var currency : String?
	var value : String?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		currency <- map["currency"]
		value <- map["value"]
	}

}
