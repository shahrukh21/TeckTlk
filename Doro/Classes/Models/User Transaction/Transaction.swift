
import Foundation
import ObjectMapper

class Transaction : Mappable {
    var id = -1
    var donorEmail = ""
    var amount = -1
    var createdAt = ""
    var recieverEmail = ""
    var batchId = ""
    var donorId = -1
    var doneeId = -1
    var donorName = ""
    var doneeName = ""
    var paymentMethod = ""
    var message = ""
    var doneeEmail = ""
    var imageURL = ""
    var received = false
    
    required init?(map: Map) { }

    func mapping(map: Map) {

        id              <- map["id"]
        donorEmail      <- map["donor_email"]
        amount          <- map["amount"]
        createdAt       <- map["created_at"]
        recieverEmail   <- map["reciever_email"]
        batchId         <- map["batch_id"]
        donorId         <- map["donor_id"]
        doneeId         <- map["donee_id"]
        donorName       <- map["donor_name"]
        doneeName       <- map["donee_name"]
        paymentMethod   <- map["payment_method"]
        message         <- map["message"]
        doneeEmail      <- map["donee_email"]
        imageURL        <- map["imageURL"]
        received        <- map["recieved"]

    }
}
