
import Foundation
import ObjectMapper

typealias GetDoneeNotificationCompletionHandler = (_ data: DoneeNotification?, _ error: Error?, _ status: Int?) -> Void

class NotificationData : Mappable {
	var error = false
	var message = ""
    var data =  Mapper<DoneeNotification>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) { }

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func getDoneeNotifications(offset: Int, _ completion: @escaping GetDoneeNotificationCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getDoneeNotifications(offset: offset) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                let json = ["data": result]
                if let data = Mapper<DoneeNotification>().map(JSON: json as [String : Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                completion(nil, error, 404)
            }
        }
    }
}
