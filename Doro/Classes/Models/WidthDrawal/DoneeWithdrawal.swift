

import Foundation
import ObjectMapper

typealias SetWidthDrawalFromDonee = (_ data: DoneeWidthDrawal?, _ error: Error?, _ status: Int?) -> Void


class DoneeWidthDrawal : Mappable {
	var error : Bool?
	var message : String?
	var data : String?
	var errors : [String]?

	required init?(map: Map) {}

    func mapping(map: Map) {
		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}
    
    class func setDoneeWidthdrawal (doneeId: Int, amount: Double, _ completion: @escaping SetWidthDrawalFromDonee) {
        Utility.showLoading()
        
        APIClient.shared.withdrawFromDoneeMethod(doneeId: doneeId, amount: amount) { (result, error, status) in
            
            Utility.hideLoading()
            
            if error == nil {
            
                let json = ["data": result]
                if let data = Mapper<DoneeWidthDrawal>().map(JSON: json as! [String: Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                
            }
            completion(nil, error, 404)
        }
    }
}
