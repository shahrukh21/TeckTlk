

import Foundation
import ObjectMapper

struct DoneeAmountData : Mappable {
	var id = -1
	var recievedAmount = 0
	var doneeId = -1
	var doneeEmail = ""
	var withdrawnAmount = 0
    var total = 0

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		recievedAmount <- map["recieved_amount"]
		doneeId <- map["donee_id"]
        total <- map["total_balance"]
		doneeEmail <- map["donee_email"]
		withdrawnAmount <- map["withdrawn_amount"]
	}

}
