
import Foundation
import ObjectMapper

typealias UpdatePaypalEmailCompletionHandler = (_ data: String?, _ error: Error?, _ status: Int?) -> Void

class UpdatePaypalEmail : Mappable {
	var error = false
	var message = ""
	var data = ""
	var errors = [""]

	required init?(map: Map) { }

    func mapping(map: Map) {

		error   <- map["error"]
		message <- map["message"]
		data    <- map["data"]
		errors  <- map["errors"]
	}
    
    class func updatePaypalEmail(oldPaypalEmail: String, newPaypalEmail: String, confirmNewPaypalEmail: String, _ completion: @escaping UpdatePaypalEmailCompletionHandler) {
    
        APIClient.shared.updatePaypalEmail (oldPaypalEmail: oldPaypalEmail, newPaypalEmail: newPaypalEmail, confirmNewPaypalEmail: confirmNewPaypalEmail) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
        
                if let data =  result as! String? {
                    
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
