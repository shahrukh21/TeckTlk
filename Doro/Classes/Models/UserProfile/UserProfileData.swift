
import Foundation
import ObjectMapper

typealias GetUserProfileCompletionHandler = (_ data: UserProfile?, _ error: Error?, _ status: Int?) -> Void
typealias GetUserProfileDataCompletionHandler = (_ data: UserProfileData?, _ error: Error?, _ status: Int?) -> Void


class UserProfileData : Mappable {
	var error = false
	var message  = ""
	var data  = Mapper<UserProfile>().map(JSON: [:])
	var errors = [""]

	required init?(map: Map) { }

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data.user"]
		errors <- map["errors"]
	}
    
    class func getUserProfile(doneeId: Int = -1, _ completion: @escaping GetUserProfileCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getUserProfile(doneeId: doneeId) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
        
                if let data = Mapper<UserProfile>().map(JSON: result as! [String : Any]) {
                    //DataManager.shared.setUser(user: data.toJSONString() ?? "")
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
    
    class func updateUserProfile(image: UIImage, name: String, email: String, phoneNo: String, nationalId: String, ssn: String, occupation: String, type: Int, _ completion: @escaping GetUserProfileDataCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.updateUserProfile(image: image, name: name, email: email, phoneNo: phoneNo, nationalId: nationalId, ssn: ssn, occupation: occupation, type: type) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
        
                if let data = Mapper<UserProfileData>().map(JSON: result as! [String : Any]) {
                    //DataManager.shared.setUser(user: data.toJSONString() ?? "")
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, nil, 201)
                }
                
            } else {
                 completion(nil, error, 404)
            }
        }
    }
}
