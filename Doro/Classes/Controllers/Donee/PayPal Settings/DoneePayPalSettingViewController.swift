//
//  DoneePayPalSettingViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class DoneePayPalSettingViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var doneePayPalEmailIDTextField: MDCTextField!
    @IBOutlet weak var doneeNewPayPalEmailIDTextField: MDCTextField!
    @IBOutlet weak var doneeConfirmPayPalEmailIDTextField: MDCTextField!
    
    
    //MARK: - Variables
    var paypalEmailController: MDCTextInputControllerOutlined?
    var newPaypalEmailController: MDCTextInputControllerOutlined?
    var confirmPaypalEmailController: MDCTextInputControllerOutlined?
    
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        paypalEmailController = MDCTextInputControllerOutlined(textInput: doneePayPalEmailIDTextField)
        newPaypalEmailController = MDCTextInputControllerOutlined(textInput: doneeNewPayPalEmailIDTextField)
        confirmPaypalEmailController = MDCTextInputControllerOutlined(textInput: doneeConfirmPayPalEmailIDTextField)
        paypalEmailController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        paypalEmailController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        newPaypalEmailController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        newPaypalEmailController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        confirmPaypalEmailController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        confirmPaypalEmailController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
    
        paypalEmailController?.borderRadius = 10.0
        newPaypalEmailController?.borderRadius = 10.0
        confirmPaypalEmailController?.borderRadius = 10.0
        
        self.doneePayPalEmailIDTextField.tag = 0
        self.doneeNewPayPalEmailIDTextField.tag = 1
        self.doneeConfirmPayPalEmailIDTextField.tag = 2
        self.doneePayPalEmailIDTextField.delegate = self
        self.doneeNewPayPalEmailIDTextField.delegate = self
        self.doneeConfirmPayPalEmailIDTextField.delegate = self

    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func updateTapped(_ sender: Any) {
        
        if doneePayPalEmailIDTextField.isValidEmail(doneePayPalEmailIDTextField.text!) &&
            doneeNewPayPalEmailIDTextField.isValidEmail(doneeNewPayPalEmailIDTextField.text!) &&
            doneeConfirmPayPalEmailIDTextField.isValidEmail(doneeConfirmPayPalEmailIDTextField.text!) {
            
            updatePayPalEmail()
            
        } else {
            showAlert(title: "", message: "Invalid email address")
        }
    }
    
    
    //MARK: - Private Methods
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    
    //MARK: - Private Methods
    private func updatePayPalEmail () {
        Utility.showLoading()
        UpdatePaypalEmail.updatePaypalEmail(oldPaypalEmail: doneePayPalEmailIDTextField.text!, newPaypalEmail: doneeNewPayPalEmailIDTextField.text!, confirmNewPaypalEmail: doneeConfirmPayPalEmailIDTextField.text!, { (result, error, status) in
            
            if error == nil {
                self.showAlert(title: "", message: result ?? "")
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
            
        })
    }
}
