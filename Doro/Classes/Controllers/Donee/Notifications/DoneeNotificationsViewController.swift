//
//  DoneeNotificationsViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class DoneeNotificationsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK: - Variables
    var notifications = Mapper<DoneeNotifications>().map(JSON: [:])!
    var offset = 0
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        dataSource()
    }
    
    
    //MARK: -IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Data Source
    func dataSource() {
       getNotifications()
    }
    
    
    //MARK: - Private Methods
    private func getNotifications (pageNo: Int = 0) {
        
        DoneeNotifications.doneeNotifications(offset: pageNo) {[weak self] (result, error, status) in
            
            if error == nil {
                self?.notifications.data.append(contentsOf: result?.data ?? [])
                self?.tableView.reloadData()
            } else {
                
            }
        }
    }
}


//MARK: - Tableview Datasource & Delegates
extension DoneeNotificationsViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.register(NotificationTableViewCell.self, indexPath: indexPath)
        cell.configure(notification: notifications.data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            offset += 1
            getNotifications(pageNo: offset)
        }
    }
}
