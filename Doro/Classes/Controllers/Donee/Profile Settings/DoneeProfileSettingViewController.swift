//
//  DoneeProfileSettingViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import SDWebImage

class DoneeProfileSettingViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var doneeEditProfileView: UIView!
    @IBOutlet weak var userProfilePicImageView: UIImageView!
    @IBOutlet weak var doneeUserNameTextField: UITextField!
    @IBOutlet weak var doneeEmailTextField: UITextField!
    @IBOutlet weak var doneePhoneNoTextField: UITextField!
    @IBOutlet weak var doneeNationalIdTextField: UITextField!
    @IBOutlet weak var doneeSocialSecurityNoTextField: UITextField!
    @IBOutlet weak var doneeOccupationTextField: UITextField!
    
    
    //MARK: - Variables
    let imagePicker = UIImagePickerController()
    var user: [String: String] = [:]
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - setupView
    func setupView() {
        userProfilePicImageView.cornerRadius = userProfilePicImageView.frame.width / 2
        doneeEditProfileView.layer.masksToBounds = false
        doneeEditProfileView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0.1607843137, alpha: 1)
        doneeEditProfileView.layer.shadowOpacity = 0.1
        doneeEditProfileView.layer.shadowOffset = CGSize(width: 0, height: 8)
        doneeEditProfileView.layer.shadowRadius = 10
        doneeEditProfileView.cornerRadius = 7
        imagePicker.delegate = self
        doneeUserNameTextField.delegate = self
        doneeEmailTextField.delegate = self
        doneePhoneNoTextField.delegate = self
        doneeNationalIdTextField.delegate = self
        doneeSocialSecurityNoTextField.delegate = self
        self.doneeUserNameTextField.tag = 0
        self.doneeEmailTextField.tag = 1
        self.doneePhoneNoTextField.tag = 2
        self.doneeNationalIdTextField.tag = 3
        self.doneeSocialSecurityNoTextField.tag = 4
        self.doneeSocialSecurityNoTextField.tag = 5
        dataSource()
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        updateUserProfile()
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        
        ImagePickerManager().pickImage(self) { (image) in
            self.userProfilePicImageView.image = image
        }
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile ({[weak self] (result, error, status) in
                
            if error == nil {
                self?.doneeUserNameTextField.text = result?.fullName
                self?.doneeEmailTextField.text = result?.email
                self?.doneePhoneNoTextField.text = result?.phoneNumber
                self?.doneeNationalIdTextField.text = result?.nationalId
                self?.doneeSocialSecurityNoTextField.text = result?.socialSecurityNumber
                self?.doneeOccupationTextField.text = result?.occupation
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!  ) {
                    self?.userProfilePicImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: "Something went wrong")
            }
        })
    }
    
    
    //MARK: - Private methods
    private func validateFields() -> Bool {
        
        if doneeUserNameTextField.text!.isEmpty ||
            doneeEmailTextField.text!.isEmpty ||
            doneePhoneNoTextField.text!.isEmpty ||
            doneeNationalIdTextField.text!.isEmpty ||
            doneeSocialSecurityNoTextField.text!.isEmpty {
            self.showAlert(title: "Error", message: "Please fill all the fields")
            return false
        }
        else if !doneeEmailTextField.isValidEmail(doneeEmailTextField.text ?? "") {
            self.showAlert(title: "Error", message: "Email Id is incorrect")
            return false
        }
        return true
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    private func updateUserProfile() {
        Utility.showLoading()
        UserProfileData.updateUserProfile(image: userProfilePicImageView.image!, name: doneeUserNameTextField.text!, email: doneeEmailTextField.text!, phoneNo: doneePhoneNoTextField.text!, nationalId: doneeNationalIdTextField.text!, ssn: doneeSocialSecurityNoTextField.text!, occupation: doneeOccupationTextField.text!, type: 0) { (data, error, status) in

            if error == nil {
                self.showAlert(title: "", message: "Profile updated successfully")
                
            } else {
                self.showAlert(title: "", message: "Error updating the profile")
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        userProfilePicImageView.image = image
        self.dismiss(animated: true, completion: nil)
    }
}



//MARK:- Textfield Delegates
extension DoneeProfileSettingViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == doneePhoneNoTextField {
            let maxLength = 13
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
//            if textField.text?.count == 0 {
//                let attributedString = NSMutableAttributedString(string: "+")
//                doneePhoneNoTextField.attributedText = attributedString
//            }
            return newString.length <= maxLength

        }
        
        if textField == doneeUserNameTextField {
            
            do {
                
                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                    
                    } else {
                        return true
                    }
            }
            catch { }
        }
        
        let maxLength = 50
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
