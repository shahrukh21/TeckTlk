//
//  DonorCardDetailsViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class DonorCardDetailsViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var donorCardNameTextField: MDCTextField!
    @IBOutlet weak var donorCardNoTextField: MDCTextField!
    @IBOutlet weak var donorCVVNoTextField: MDCTextField!
    @IBOutlet weak var donorExpiryDateTextField: MDCTextField!
    
    
    //MARK: - Variables
    var donorCardNameController: MDCTextInputControllerOutlined?
    var donorCardNoController: MDCTextInputControllerOutlined?
    var donorCVVNoController: MDCTextInputControllerOutlined?
    var donorExpiryDateController: MDCTextInputControllerOutlined?
    var window : UIWindow?
    let datePicker = UIDatePicker()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - Setup View
    func setupView() {
        donorCardNameController = MDCTextInputControllerOutlined(textInput: donorCardNameTextField)
        donorCardNameController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        donorCardNameController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        donorCardNoController = MDCTextInputControllerOutlined(textInput: donorCardNoTextField)
        donorCardNoController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        donorCardNoController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        donorCVVNoController = MDCTextInputControllerOutlined(textInput: donorCVVNoTextField)
        donorCVVNoController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        donorCVVNoController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        donorExpiryDateController = MDCTextInputControllerOutlined(textInput: donorExpiryDateTextField)
        donorExpiryDateController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        donorExpiryDateController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        showDatePicker()
        donorCardNameController?.borderRadius = 10.0
        donorCardNoController?.borderRadius = 10.0
        donorCVVNoController?.borderRadius = 10.0
        donorExpiryDateController?.borderRadius = 10.0
        self.donorCardNameTextField.tag = 0
        self.donorCardNoTextField.tag = 1
        self.donorCVVNoTextField.tag = 2
        self.donorExpiryDateTextField.tag = 3
        self.donorCardNameTextField.delegate = self
        self.donorCardNoTextField.delegate = self
        self.donorCVVNoTextField.delegate = self
        self.donorExpiryDateTextField.delegate = self

    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        let rootVC = DonorTabBarViewController(nibName: "DonorTabBarViewController", bundle: nil)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
    }
    @IBAction func skipTapped(_ sender: Any) {
        let rootVC = DonorTabBarViewController(nibName: "DonorTabBarViewController", bundle: nil)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
        skipButtonTapped = true
    }
    
    @IBAction func loginButton(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
                 if controller.isKind(of: LoginViewController.self) {
                     self.navigationController!.popToViewController(controller, animated: true)
                     break
                     }
                 }
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        donorExpiryDateTextField.inputAccessoryView = toolbar
        donorExpiryDateTextField.inputView = datePicker
        }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/yyyy"
        donorExpiryDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
 
}
