//
//  SignupPaymentViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class SignupPaymentViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var selection1: UIImageView!
    @IBOutlet weak var selection2: UIImageView!
    @IBOutlet weak var selection3: UIImageView!
    @IBOutlet weak var venmoPaymentView: UIView!
    @IBOutlet weak var creditCardPaymentView: UIView!
    
    
    //MARK: - Variables
    var paymentMethod : String = ""
    var window : UIWindow?
    var user : [String: String]?
    //  var skipButtonTapped = false
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(self.user!)
    }
    
    //MARK: - View Setup
    func setupView() {
        
    }
    
    
    //MARK: - IBActions
    @IBAction func paymentOptionTapped(_ sender: UIButton) {
        //sender.isSelected = true
        if sender.tag == 0 {
            paymentMethod = "paypal"
            selection1.image = #imageLiteral(resourceName: "selected")
            selection2.image = #imageLiteral(resourceName: "unselected")
            selection3.image = #imageLiteral(resourceName: "unselected")
            
        } else if sender.tag == 1 {
            paymentMethod = "venmo"
            selection1.image = #imageLiteral(resourceName: "unselected")
            selection2.image = #imageLiteral(resourceName: "selected")
            selection3.image = #imageLiteral(resourceName: "unselected")
            
        } else {
            paymentMethod = "creditcard"
            selection1.image = #imageLiteral(resourceName: "unselected")
            selection2.image = #imageLiteral(resourceName: "unselected")
            selection3.image = #imageLiteral(resourceName: "selected")
        }
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        if paymentMethod == "paypal" {
            let paypalVC = PaypalDetailsViewController(nibName: "PaypalDetailsViewController", bundle: nil)
            self.navigationController?.pushViewController(paypalVC, animated: true)
            
        } else if paymentMethod == "venmo" {
            let venmoVC = VenmoDetailsViewController(nibName: "VenmoDetailsViewController", bundle: nil)
            self.navigationController?.pushViewController(venmoVC, animated: true)
            
        } else if paymentMethod == "creditcard" {
            let creditCardVC = DonorCardDetailsViewController(nibName: "DonorCardDetailsViewController", bundle: nil)
            self.navigationController?.pushViewController(creditCardVC, animated: true)
        }
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        createUser(user: apiRequestObject)
    }
    
    @IBAction func backTappped(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SignUpViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    //MARK: - Private Methods
    
    @IBAction func loginButton(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}


//MARK: - APICalls
extension SignupPaymentViewController {
    
    private func createUser (user: [String: String]) {
        Utility.showLoading()
        
        UserData.createUser(user: user) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                Utility.setDonorTabAsRootViewController()
                
            } else {
                self.showAlert(title: "", message: "Could not be able create user.")
            }
        }
    }
}

