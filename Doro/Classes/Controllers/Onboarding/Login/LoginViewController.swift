//
//  LoginViewController.swift
//  Doro
//
//  Created by a on 02/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailTextField: MDCTextField!
    @IBOutlet weak var passwordTextField: MDCTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    
    //MARK:- Variables
    var apiData: String?
    var emailController: MDCTextInputControllerOutlined?
    var passwordController: MDCTextInputControllerOutlined?
    private var loginViewModel : LoginViewModel!
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - SetupView
    func setupView() {
        
        self.navigationController?.navigationBar.isHidden = true
        emailController = MDCTextInputControllerOutlined(textInput: emailTextField)
        emailController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        emailController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        
        passwordController = MDCTextInputControllerOutlined(textInput: passwordTextField)
        passwordController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        passwordController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        self.emailTextField.tag = 0
        self.passwordTextField.tag = 1
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        emailController?.borderRadius = 10.0
        passwordController?.borderRadius = 10.0
        
    }
    
    
    //MARK: - IBActions
    
    @IBAction func forgotPasswordTapped(_ sender: Any) {
       showForgotPasswordScreen()
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        loginAPI()
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        let signupVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    
    //MARK: - Private Methods
    func dataSource(){
        
        self.loginViewModel =  LoginViewModel()
        Utility.showLoading()
        self.loginViewModel.bindLoginViewModelToController = {
            self.updateDataSource()
        }
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    func updateDataSource(){
        
        self.apiData = loginViewModel.loginData
        DispatchQueue.main.async {
            Utility.hideLoading()
            self.label.text = self.apiData
            //            self.tableView.delegate = self
            //            self.tableView.reloadData()
        }
    }
    
    
    private func showForgotPasswordScreen () {
        let vc = ForgotPasswordViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - APICalls
    private func loginAPI () {
        
        if emailTextField.isValidEmail(emailTextField.text ?? "") && passwordTextField.text != "" {
            
            if passwordTextField.text?.count ?? 0 < 9 {
                self.showAlert(title: "", message: "Password should be greater than 8 characters.")
                return
            }
                
            Utility.showLoading()
            UserData.login(email: emailTextField.text!, password: passwordTextField.text!, { (result, error, status) in
                
                if result != nil {
                    UserData.shared = result!
                    if result?.user.userType == "donor" {
                        Utility.setDonorTabAsRootViewController()
                        
                    } else {
                        Utility.setDoneeTabAsRootViewController()
                    }
                    
                } else if result == nil {
                    self.showAlert(title: "", message: error?.localizedDescription ?? "")
                    
                } else {
                    self.showAlert(title: "", message: error?.localizedDescription ?? "")
                }
            })
            
        } else {
            showAlert(title: "", message: "Invalid Email or Password")
        }
        
    }
}
