//
//  AssistantViewController.swift
//  Doro
//
//  Created by Mapple Technologies on 01/12/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

enum Option {
    case showBlutooth
    case showAssistant
    case showPlayer
    case showGoogle
    case showMap
    case showInsta
    case showFacebook
    case showContact
}

class AssistantViewController: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var callingView: UIView!
    @IBOutlet weak var socialMediaView: UIView!
    @IBOutlet weak var optionChangeImageView: UIImageView!
    @IBOutlet weak var optionChangeLabel: UILabel!
    @IBOutlet weak var plusminusStackView: UIStackView!
    @IBOutlet weak var showMapStackView: UIStackView!
    @IBOutlet weak var assistantImageView: UIImageView!
    @IBOutlet weak var optionIconImageView: UIImageView!
    @IBOutlet weak var assistantView: UIView!
    @IBOutlet weak var carPlayStackView: UIStackView!
    @IBOutlet weak var headsetStackView: UIStackView!
    @IBOutlet weak var assitantView: NSLayoutConstraint!
    @IBOutlet weak var mobileStackView: UIStackView!
    
    
    //MARK: - Variables
    var mode: Option = .showAssistant
    
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(openAssistant))
        logoImageView.addGestureRecognizer(tap)
        
    }
    
    
    //MARK: - Selectors
    @objc func openAssistant () {
        mode = .showMap
        optionPressed("")
    }
    
    
    //MARK: - Actions
    @IBAction func optionPressed(_ sender: Any) {
        plusminusStackView.isHidden = true
        showMapStackView.isHidden = true
        assitantView.constant = 60
        assistantView.isHidden = false
        hideBlutoothOptions(show: true)
        socialMediaView.isHidden = true
        callingView.isHidden = true

        switch mode {
        
        case .showAssistant:
            mode = .showBlutooth
            hideBlutoothOptions(show: false)
            optionIconImageView.image = #imageLiteral(resourceName: "Icon material-keyboard-voice-big")
            assitantView.constant = 0
            assistantView.isHidden = true
            
        case .showBlutooth:
            mode = .showPlayer
            optionIconImageView.image = #imageLiteral(resourceName: "Icon material-bluetooth-audio")
            assistantImageView.image = #imageLiteral(resourceName: "Icon feather-youtube")
            plusminusStackView.isHidden = false
            
        case .showPlayer:
            mode = .showGoogle
            optionIconImageView.image = #imageLiteral(resourceName: "Icon material-bluetooth-audio")
            assistantImageView.image = #imageLiteral(resourceName: "Icon ionic-logo-google")
            plusminusStackView.isHidden = false
            
        case .showGoogle:
            mode = .showMap
            optionIconImageView.image = #imageLiteral(resourceName: "Icon material-bluetooth-audio")
            assistantImageView.image = #imageLiteral(resourceName: "Repeat Grid 1")
            showMapStackView.isHidden = false
            
        case .showMap:
            mode = .showAssistant
            assistantView.isHidden = false
            assistantImageView.image = #imageLiteral(resourceName: "Icon material-keyboard-voice-green")
            assitantView.constant = 60
            optionIconImageView.image = #imageLiteral(resourceName: "Icon material-bluetooth-audio")
                        
        default:
            break;
        }
        self.view.layoutIfNeeded()
    }
    
    @IBAction func facebookPressed(_ sender: Any) {
        mode = .showFacebook
        defaultHiddenViews ()
        
        assitantView.constant = 0
        assistantView.isHidden = true
        carPlayStackView.isHidden = false
        optionChangeLabel.text = "Connect Sentia"
        optionChangeImageView.image = #imageLiteral(resourceName: "Icon feather-facebook")
        socialMediaView.isHidden = false

    }
    
    @IBAction func instaPressed(_ sender: Any) {
        mode = .showInsta
        defaultHiddenViews ()
        
        assitantView.constant = 0
        assistantView.isHidden = true
        carPlayStackView.isHidden = false
        optionChangeLabel.text = "Connect Sentia"
        optionChangeImageView.image = #imageLiteral(resourceName: "Icon ionic-logo-instagram")
        socialMediaView.isHidden = false
    }
    
    @IBAction func contactPressed(_ sender: Any) {
        mode = .showContact
        defaultHiddenViews ()
        
        assitantView.constant = 0
        assistantView.isHidden = true
        carPlayStackView.isHidden = false
        optionChangeLabel.text = "Call Sentia"
        optionChangeImageView.image = #imageLiteral(resourceName: "Icon ionic-ios-contacts")
        callingView.isHidden = false
    }
    
    //MARK:- Private methods
    func hideBlutoothOptions (show: Bool) {
        carPlayStackView.isHidden = show
        headsetStackView.isHidden = show
        mobileStackView.isHidden = show
    }

    func defaultHiddenViews () {
        plusminusStackView.isHidden = true
        showMapStackView.isHidden = true
        assitantView.constant = 60
        assistantView.isHidden = false
        hideBlutoothOptions(show: true)
        socialMediaView.isHidden = true
        callingView.isHidden = true
    }
}
