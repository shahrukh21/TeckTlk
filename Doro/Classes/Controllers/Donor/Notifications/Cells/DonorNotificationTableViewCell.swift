//
//  DonorNotificationTableViewCell.swift
//  Doro
//
//  Created by Macbook on 22/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DonorNotificationTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var notificationDescriptionlabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ellipse: UIImageView!
    @IBOutlet weak var ellipseImage: NSLayoutConstraint!
    
    
    //MARK: - Variables
    
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        notificationImage.cornerRadius = notificationImage.frame.width / 2
        ellipse.isHidden = true
    }
    
    
    //MARK: - Configure
    func configure (notification: DonorNotificationData) {
        notificationImage.sd_setImage(with: URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl.trimmingCharacters(in: .whitespacesAndNewlines) + notification.imageURL.trimmingCharacters(in: .whitespacesAndNewlines))! , placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
        notificationDescriptionlabel.text =  notification.message
        timeLabel.text = Utility.dataInEnglish(notification.createdAt)
    }
}
