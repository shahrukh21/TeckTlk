//
//  DonorProfileViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class DonorProfileViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var donorProfileImage: UIImageView!
    @IBOutlet weak var donorNameLabel: UILabel!
    @IBOutlet weak var donorOccupationLabel: UILabel!
    @IBOutlet weak var uiSwitch: UISwitch!
    
    
    //MARK: - Variables
    var window : UIWindow?
    var donorNotificationVC = DonorNotificationsViewController()
    var donorProfileSettingVC = DonorProfileSettingViewController()
    var donorselectPaymentVC = SelectPaymentViewController()
    var loginVC = LoginViewController()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        dataSource()
    }
    
    
    //MARK: - Setup View
    func setupView() {
        self.navigationController?.navigationBar.isHidden = true
        donorProfileImage.cornerRadius = donorProfileImage.frame.width / 2
    }
    
    
    //MARK: - IBActions
    @IBAction func notificationTapped(_ sender: Any) {
        donorNotificationVC = DonorNotificationsViewController()
        self.navigationController?.pushViewController(donorNotificationVC, animated: true)
    }
    
    @IBAction func profileSettingsTapped(_ sender: Any) {
        donorProfileSettingVC = DonorProfileSettingViewController()
        self.navigationController?.pushViewController(donorProfileSettingVC, animated: true)
    }
    
    @IBAction func defaultPaymentPressed(_ sender: Any) {
        donorselectPaymentVC = SelectPaymentViewController()
        donorselectPaymentVC.isChangePaymentGateway = true
        self.navigationController?.pushViewController(donorselectPaymentVC, animated: true)
    }
    
    @IBAction func paymentMethodTapped(_ sender: Any) {
        donorselectPaymentVC = SelectPaymentViewController()
        self.navigationController?.pushViewController(donorselectPaymentVC, animated: true)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        DataManager.shared.deleteUser()
        Utility.loginRootViewController()
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile ({[weak self] (result, error, status) in
                
            if error == nil {
                self?.donorNameLabel.text = result?.fullName
                self?.donorOccupationLabel.text = result?.occupation
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!) {
                    self?.donorProfileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: "Something went wrong")
            }
        })
    }
}
