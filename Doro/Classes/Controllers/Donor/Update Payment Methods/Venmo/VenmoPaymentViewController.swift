//
//  VenmoPaymentViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class VenmoPaymentViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var venmoEmailIDTextField: MDCTextField!
    @IBOutlet weak var newVenmoEmailIDTextField: MDCTextField!
    @IBOutlet weak var confirmVenmoEmailIDTextField: MDCTextField!
    
    
    //MARK: - Variables
    var venmoEmailIdController: MDCTextInputControllerOutlined?
    var newVenmoEmailIdController: MDCTextInputControllerOutlined?
    var confirmVenmoEmailIdController: MDCTextInputControllerOutlined?
    
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        venmoEmailIdController = MDCTextInputControllerOutlined(textInput: venmoEmailIDTextField)
        newVenmoEmailIdController = MDCTextInputControllerOutlined(textInput: newVenmoEmailIDTextField)
        confirmVenmoEmailIdController = MDCTextInputControllerOutlined(textInput: confirmVenmoEmailIDTextField)
        venmoEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        venmoEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        newVenmoEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        newVenmoEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        confirmVenmoEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        confirmVenmoEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        venmoEmailIdController?.borderRadius = 10.0
        newVenmoEmailIdController?.borderRadius = 10.0
        confirmVenmoEmailIdController?.borderRadius = 10.0
        
        self.venmoEmailIDTextField.tag = 0
        self.newVenmoEmailIDTextField.tag = 1
        self.confirmVenmoEmailIDTextField.tag = 2
        self.venmoEmailIDTextField.delegate = self
        self.newVenmoEmailIDTextField.delegate = self
        self.confirmVenmoEmailIDTextField.delegate = self
    
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        
        if venmoEmailIDTextField.isValidEmail(venmoEmailIDTextField.text!) && newVenmoEmailIDTextField.isValidEmail(newVenmoEmailIDTextField.text!) && confirmVenmoEmailIDTextField.isValidEmail(confirmVenmoEmailIDTextField.text!) {
            
            if newVenmoEmailIDTextField.text == confirmVenmoEmailIDTextField.text {
                updateCustomerEmail ()
            }
        }
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }

    func updateCustomerEmail () {
        
        APIClient.shared.updateStripeEmail(customerId: (DataManager.shared.getUser()?.user.stripeCustId)!, newEmail: newVenmoEmailIDTextField.text ?? "", oldEmail: venmoEmailIDTextField.text ?? "") { (result, error, status) in
            
            if error == nil {
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DonorProfileViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}
