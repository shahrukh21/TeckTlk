//
//  DonorProfileSettingViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DonorProfileSettingViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var donorEditProfileView: UIView!
    @IBOutlet weak var donorImage: UIImageView!
    @IBOutlet weak var donorNameTextField: UITextField!
    @IBOutlet weak var donorEmailTextField: UITextField!
    @IBOutlet weak var donorPhoneNoTextField: UITextField!
    @IBOutlet weak var donorOccupationTextField: UITextField!

    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        donorEditProfileView.layer.masksToBounds = false
        donorEditProfileView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0.1607843137, alpha: 1)
        donorEditProfileView.layer.shadowOpacity = 0.1
        donorEditProfileView.layer.shadowOffset = CGSize(width: 0, height: 8)
        donorEditProfileView.layer.shadowRadius = 10
        donorEditProfileView.cornerRadius = 7
        
        donorImage.cornerRadius = donorImage.frame.width / 2
        donorNameTextField.delegate = self
        donorEmailTextField.delegate = self
        donorPhoneNoTextField.delegate = self
        dataSource()
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        
        if donorEmailTextField.isValidEmail(donorPhoneNoTextField.text!) {
            self.showAlert(title: "", message: "Please enter a valid email addess")
            
        } else {
            updateUserProfile()
        }
    }
    
    @IBAction func cameraButton(_ sender: Any) {
        ImagePickerManager().pickImage(self) { (image) in
            self.donorImage.image = image
        }
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile ({[weak self] (result, error, status) in
                
            if error == nil {
                self?.donorNameTextField.text = result?.fullName
                self?.donorEmailTextField.text = result?.email
                self?.donorPhoneNoTextField.text = result?.phoneNumber
                self?.donorOccupationTextField.text = result?.occupation
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!  ) {
                    self?.donorImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: "Something went wrong")
            }
        })
    }
    
    
    //MARK: - Private Methods
    private func updateUserProfile() {

        UserProfileData.updateUserProfile(image: donorImage.image ?? #imageLiteral(resourceName: "profileImage"), name: donorNameTextField.text ?? "", email: donorEmailTextField.text ?? "", phoneNo: donorPhoneNoTextField.text ?? "", nationalId: "", ssn: "", occupation: donorOccupationTextField.text ?? "", type: 1) { (data, error, status) in
            
            if error == nil {
                self.showAlert(title: "", message: "Profile updated successfully")
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK:- Textfield Delegates
extension DonorProfileSettingViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == donorPhoneNoTextField {
            let maxLength = 13
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength

        }

        if textField == donorNameTextField {
            
            do {
                
                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                    
                    } else {
                        return true
                    }
            }
            catch { }
        }
        
        let maxLength = 50
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
