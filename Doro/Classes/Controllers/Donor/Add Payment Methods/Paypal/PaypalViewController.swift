//
//  PaypalViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class PaypalViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var paypalEmailIdTextField: MDCTextField!
    @IBOutlet weak var confirmPayPalEmailIdTextField: MDCTextField!
    
    //MARK: - Variables
    var paypalEmailIdController: MDCTextInputControllerOutlined?
    var confirmPaypalEmailIdController: MDCTextInputControllerOutlined?
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        paypalEmailIdController = MDCTextInputControllerOutlined(textInput: paypalEmailIdTextField)
        confirmPaypalEmailIdController = MDCTextInputControllerOutlined(textInput: confirmPayPalEmailIdTextField)
        paypalEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        paypalEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        confirmPaypalEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        confirmPaypalEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        paypalEmailIdController?.borderRadius = 10.0
        confirmPaypalEmailIdController?.borderRadius = 10.0
        
        self.paypalEmailIdTextField.tag = 0
        self.confirmPayPalEmailIdTextField.tag = 1
        self.paypalEmailIdTextField.delegate = self
        self.confirmPayPalEmailIdTextField.delegate = self
        
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func doneTapped(_ sender: Any) {
        createPaypalPayment()
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1
        
        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    
    func createPaypalPayment () {
        
        if !paypalEmailIdTextField.isValidEmail(paypalEmailIdTextField.text!) {
            self.showAlert(title: "", message: "Please enter a valid email addess")
            return
        }
        
        if !confirmPayPalEmailIdTextField.isValidEmail(confirmPayPalEmailIdTextField.text!) {
            self.showAlert(title: "", message: "Please enter a valid email addess")
            return
        }
        
        if paypalEmailIdTextField.text == confirmPayPalEmailIdTextField.text {
            createPaypalAccount ()
            
        } else {
            self.showAlert(title: "", message: "Confirm stripe email does not match above.")
        }
    }
    
    private func createPaypalAccount () {
        
        UserData.updatePaymentEmail(email: paypalEmailIdTextField.text ?? "", paymentMethod: "paypal") { (result, error, status) in
            
            if error == nil {
                
                if UserData.shared.doneeIdForWithdrawal != -1 {
                    let creditCardVC = DonorTransferAmountViewController()
                    self.navigationController?.pushViewController(creditCardVC, animated: true)
                    
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
            } else {
                self.showAlert(title: "", message: "Stripe Account not created!")
            }
        }
    }
}
